from django.shortcuts import render

from . import util

import markdown2

from django.urls import reverse

from django.http import HttpResponseRedirect

from django import forms

import random


class NewPage(forms.Form):
    title = forms.CharField(label="Title", max_length=100, widget=forms.TextInput(attrs={'placeholder':"Title"}))
    new_entry = forms.CharField(label="New Entry", widget=forms.Textarea(attrs={'placeholder':"Content"}))


def index(request):
    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })

def pages(request, title):
    
    content = util.get_entry(title)
    if content == None:
        return render(request, "encyclopedia/404.html")

    else:
        return render(request, "encyclopedia/pages.html", {
            "content": markdown2.markdown(content),
            "title": title
    })

def search(request):
    query = request.POST["q"]
    titles = util.list_entries()

    if query in titles:
        return HttpResponseRedirect(reverse("pages", kwargs={"title": query}))

    else:
        List = []
        for name in titles:
            if query.lower() in name.lower():
                List.append(name)

        return render(request, "encyclopedia/results.html", {
            "List":List
        })


def new_page(request):
    if request.method == "POST":
        title = request.POST["title"]
        content = request.POST["new_entry"]

        if util.get_entry(title):
            return render(request, "encyclopedia/new_page.html", {
                "error" : 'An encyclopedia entry with the same title already exists.',
                "title": title,
                "content": content
            })
        
        else:
            util.save_entry(title,content)
            return HttpResponseRedirect(reverse("pages", kwargs={"title": title}))

    return render(request, "encyclopedia/new_page.html", {
        "form":NewPage()
    })

def edit(request,title):
    content = util.get_entry(title)
    if request.method == "GET":
        return render(request, "encyclopedia/edit.html", {
            "content": content,
            "title": title
        })

    edit_entry = request.POST["edit_entry"]

    util.save_entry(title,edit_entry)

    return HttpResponseRedirect(reverse("pages", kwargs={"title": title}))

def random_page(request):
    entries = util.list_entries()
    page = random.choice(entries)
    return HttpResponseRedirect(reverse("pages", kwargs={"title": page}))
